package view

import (
	"fmt"
	"typing-bubbletea/model"
	"github.com/charmbracelet/lipgloss"
	"golang.org/x/crypto/ssh/terminal"
)


func View(m model.Model) string {
	var content string

	switch m.Mode {
	case model.ModeGreeter:
		content = "Welcome to the typing test!\nPress any key to begin..."
	case model.ModeTyping:
		content = m.SampleText + "\n\n" + m.Input
	case model.ModeResults:
		content = fmt.Sprintf("Test completed!\n\nWPM: %.2f\nErrors: %.2f%%", m.WPM, m.ErrorRate)
	default:
		content = ""
	}

	width, height, err := terminal.GetSize(0)
	if err != nil {
		// Handle this error appropriately for your application, or default to some size.
		width = 80
		height = 24
	}
	contentWidth := lipgloss.Width(content)
	contentHeight := lipgloss.Height(content)

	horizPadding := (width - contentWidth) / 2
	vertPadding := (height - contentHeight) / 2

	style := lipgloss.NewStyle().
		PaddingTop(vertPadding).
		PaddingBottom(vertPadding).
		PaddingLeft(horizPadding).
		PaddingRight(horizPadding)

	return  style.Render(content)
}
