package model

import (
    tea "github.com/charmbracelet/bubbletea"
    "time"
)

type Model struct {
	Mode       AppMode
	StartTime  time.Time
	Input      string
	SampleText string
	WPM        float64
	ErrorRate  float64
}

type AppMode int

const (
	ModeGreeter AppMode = iota
	ModeTyping
	ModeResults
)
func (m Model) Init() tea.Cmd {
	return nil
}

func (m *Model) CalculateMetrics() {
	m.WPM = CalculateWPM(m.Input, time.Since(m.StartTime).Minutes())
	m.ErrorRate = CalculateErrorRate(m.Input, m.SampleText)
}

