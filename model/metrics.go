package model

import "strings"

func CalculateWPM(input string, elapsedTime float64) float64 {
	typedWords := len(strings.Fields(input))
	return float64(typedWords) / elapsedTime
}

func CalculateErrorRate(input, sample string) float64 {
	errors := 0
	for i, char := range input {
		if i >= len(sample) || char != rune(sample[i]) {
			errors++
		}
	}
	return float64(errors) / float64(len(input)) * 100
}

