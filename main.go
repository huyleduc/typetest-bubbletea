package main

import (
	"typing-bubbletea/model"
	"typing-bubbletea/update"
	"typing-bubbletea/view"
	tea "github.com/charmbracelet/bubbletea"
)

type App struct {
	model.Model
}


func (a App) Init() tea.Cmd {
	return nil
}

func (a App) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	newModel, cmd := update.Update(a.Model, msg)
	return App{newModel}, cmd
}

func (a App) View() string {
	return view.View(a.Model)
}

func main() {
	app := App{
		Model: model.Model{
			Mode:       model.ModeGreeter,
			SampleText: "The quick brown fox jumps over the lazy dog.",
		},
	}

	program := tea.NewProgram(app)
	if err := program.Start(); err != nil {
		panic(err)
	}
}

