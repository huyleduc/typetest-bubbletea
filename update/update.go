package update

import (
    "time"
	"typing-bubbletea/model"
	tea "github.com/charmbracelet/bubbletea"
)
func Update(m model.Model, msg tea.Msg) (model.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch m.Mode {
		case model.ModeGreeter:
			if msg.Type == tea.KeyRunes {
				m.Mode = model.ModeTyping
				m.StartTime = time.Now()
			}
		case model.ModeTyping:
			switch msg.Type {
			case tea.KeyRunes:
				m.Input += string(msg.Runes)
			case tea.KeyBackspace, tea.KeyDelete:
				if len(m.Input) > 0 {
					m.Input = m.Input[:len(m.Input)-1]
				}
			}
			if msg.String() == "enter" {
				m.Mode = model.ModeResults
				m.CalculateMetrics()
			}
		case model.ModeResults:
			// Handle results mode key presses if needed
		}
		if msg.String() == "ctrl+c" {
			return m, tea.Quit
		}
	}
	return m, nil
}

